

<?php
include("config.php");
include("utils.php");
?>
<!doctype html>
<html>
<head>
    <title>Pendu App: Interface d’admin</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php
// On vérifie avoir récupéré en POST un paramètre joueur
// On vérifie que c’est une chaîne de caractères
if (isset($_POST["nom"]) && is_string($_POST["nom"])) {
    // On vérifie si le joueur n’existe pas déjà
    // À faire vous même !

    // On insère le joueur
    $query = "INSERT INTO Joueurs (nom) VALUES (\"" . $_POST["nom"] . "\")";
    $result = mysqli_query($handle,$query);
    // On vérifie que le joueur a bien été enregistré
    if($handle->affected_rows > 0) {
        echo "Le joueur " . $_POST["nom"] . " a été créé<br>";
    }
    else {
        echo "Une erreur est survenue lors de l’enregistrement du joueur " . $_POST["nom"] . "<br>";
    }
    echo "Retour à <a href=\"admin.php\">la page d’administration</a>";
}
else {
    echo "Un joueur doit être transmis avec cette page.<br> Vous devez accéder à cette page depuis le formulaire présent sur <a href=\"admin.php\">la page d’administration</a>";
}
?>
</body>
</html>
