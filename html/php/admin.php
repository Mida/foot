<?php
 include("config.php");
 include("utils.php");
 ?>
 <!doctype html>
 <html>
 <head>
     <title>Pendu App: Interface d’admin</title>
     <style>
         body {
                font-family: helvetica, sans;
                font-size: 24dip;
                color: #222;
                background-color: #ddd;
         }
     </style>
     <link rel="stylesheet" href="style.css">
 </head>
 <body>
 <ul>
 <?php
 ini_set("display_errors","true");
 $handle = mysqli_connect("localhost","root","","penduapp");
 $query = "SELECT * FROM Mots";
 $result = mysqli_query($handle,$query);
 while($line = mysqli_fetch_array($result)) {
     echo "\t<li>";
     echo "[" . $line["id"] . "] ";
     echo $line["mot"];
     echo "&nbsp;<a href=\"delete_mot.php?id=" . $line["id"] . "\">X</a>";
     echo "</li>\n";
 }
 ?>
 </ul>
 <form method="post" action="create_mot.php">
     <label for="mot">Nouveau mot:&nbsp;</label>
     <input name="mot" type="text" placeholder="nouveau mot">
     <input type="submit">
 </form>
 <div id="mots">
     <ul>

 <div id="joueurs">
     <ul>
     <?php
     $query = "SELECT * FROM Joueurs";
     $result = mysqli_query($handle,$query);
     while($line = mysqli_fetch_array($result)) {
         echo "\t\t<li>";
         echo $line["nom"];
         echo "&nbsp;<a href=\"delete_joueur.php?id=" . $line["id"] . "\">X</a>";
         echo "</li>\n";
     }
     ?>
     </ul>
     <form method="post" action="create_joueur.php">
         <label for="joueur">Nouveau joueur:&nbsp;</label>
         <input name="joueur" type="text" placeholder="nouveau joueur">
         <input type="submit">
     </form>
 </div>
 <form method="post" action="update_mot.php">
     <label for="joueur">Changer le mot:&nbsp;</label>
     <input name="joueur" type="text" placeholder="Update">
     <input type="submit">
 </form>
 </body>
 </html>
