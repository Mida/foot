

<?php
include("config.php");
include("utils.php");
?>
<!doctype html>
<html>
<head>
    <title>Pendu App: Interface d’admin</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php

// On vérifie avoir récupéré en POST un paramètre mot
// On vérifie que c’est une chaîne de caractères
if (isset($_POST["mot"]) && is_string($_POST["mot"])) {
    // On vérifie si le mot n’existe pas déjà
    // À faire vous même !

    // On insère le mot
    $query = "INSERT INTO Mots (mot) VALUES (\"" . $_POST["mot"] . "\")";
    $result = mysqli_query($handle,$query);
    // On vérifie que le mot a bien été enregistré
    if($handle->affected_rows > 0) {
        echo "Le mot " . $_POST["mot"] . " a été créé<br>";
    }
    else {
        echo "Une erreur est survenue lors de l’enregistrement du mot " . $_POST["mot"] . "<br>";
    }
    echo "Retour à <a href=\"admin.php\">la page d’administration</a>";
}
else {
    echo "Un mot doit être transmis avec cette page.<br> Vous devez accéder à cette page depuis le formulaire présent sur <a href=\"admin.php\">la page d’administration</a>";
}
?>
</body>
</html>
