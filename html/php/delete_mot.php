

<?php
include("config.php");
include("utils.php");
?>
<!doctype html>
<html>
<head>
    <title>Pendu App: Interface d’admin</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php
// on contrôle que le paramètre id est défini
// et que c’est bien un nombre)
if (isset($_GET["id"]) && is_numeric($_GET["id"])) {
    // On récupère des informations sur le mot
    $query = "SELECT * FROM Mots WHERE id = " . $_GET["id"];
    $result = mysqli_query($handle,$query);
    // On vérifie que le mot demandé existe
    if ($result->num_rows > 0) {
        // On récupère le mot en lui-même
        $line = mysqli_fetch_array($result);
        $mot_a_supprimer = $line["mot"];
        // On effecture la suppression
        $query = "DELETE FROM Mots WHERE id = " . $_GET["id"];
        $result = mysqli_query($handle,$query);
        // On vérifie que tout s’est bien passé
        if ($handle->affected_rows > 0) {
            // On affiche qu’on a supprimé le mot
            echo "Mot " . $mot_a_supprimer . " supprimé<br>\n";
        }
        else {
            // On affiche qu’il y a eu un problème
            echo "Le mot " . $mot_a_supprimer . " existe mais la suppression n’a pas marché<br>\n";
        }
    }
    else {
        echo "Le mot demandé n’existe pas<br>\n";
    }
}
else {
    echo "Veuillez indiquer la variable id ou vérifier qu’il
        s’agit bien d’un nombre";
}
    echo "<a href=\"admin.php\">retour</a>";
?>
</body>
</html>
