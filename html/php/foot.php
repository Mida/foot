

<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title>Liste Football</title>
</head>

<body>

  <?php
  #ini_set("display_errors","true");
  $handle=mysqli_connect("localhost","root","","foot");
  if($handle) {
    echo "Connexion OK"."<br>";
  }
  else {
    echo "Connexion HS"."<br>";
    echo mysqli_connect_error();
  }
  ?>

  <div>
    <?php
    $query="SELECT * FROM equipes";
    $result=mysqli_query($handle,$query);
    echo "<ul>";
    while($line=mysqli_fetch_array($result)) {
      echo "\t<li>";
      echo "[".$line["id"]."] ".$line["pays"]." ".$line["surnom"];
      echo "<a href=\"eq-suppr.php?id=" . $line["id"] . "\"> Supprimer </a>";
      echo "|";
      echo "<a href=\"eq-mod.php?id=" . $line["id"] . "\"> Modifier </a>";
      echo "</li>\n";
    }
    echo "</ul>";
    if($result) {
      echo "Résultat OK"."<br>";
    }
    else {
      echo "Résultat HS"."<br>";
      echo mysqli_error($handle);
    }
    ?>
  </div>

  <form action="eq-ajout.php" method="post">
    <label for="eqpays">Pays</label>
    <input type="text" name="pays">
    <label for="eqsurnom">Surnom</label>
    <input type="text" name="surnom">
    <input type="submit" value="Ajouter">
  </form>

  <div>
    <?php
    $query="SELECT * FROM joueurs";
    $result=mysqli_query($handle,$query);
    echo "<ul>";
    while($line=mysqli_fetch_array($result)) {
      echo "\t<li>";
      echo "[".$line["id"]."] ".$line["prenom"]." ".$line["nom"]." ".$line["datenaiss"]." ".$line["idequipe"];
      echo "<a href=\"eq-suppr.php?id=" . $line["id"] . "\"> Supprimer </a>";
      echo "|";
      echo "<a href=\"eq-mod.php?id=" . $line["id"] . "\"> Modifier </a>";
      echo "</li>\n";
    }
    echo "</ul>";
    if($result) {
      echo "Résultat OK"."<br>";
    }
    else {
      echo "Résultat HS"."<br>";
      echo mysqli_error($handle);
    }
    ?>
  </div>

  <form action="eq-ajout.php" method="post">
    <label for="prenom">Prenom</label>
    <input type="text" name="prenom">
    <label for="nom">Nom</label>
    <input type="text" name="nom">
    <label for="date">Date De Naissance</label>
    <input type="text" name="date">
    <input type="submit" value="Ajouter">
  </form>

</body>

</html>
